package main

import (
	"fmt"
	"strings"
	"math/rand"
	"time"
)

func randWord(words map[string]string) string {
	keys := []string{}
	for k := range words {
		keys = append(keys, k)
	}

	rand.Seed(time.Now().UnixNano())
	r := rand.Intn(len(words))

	return keys[r]
}

func riddleWord(word string) (string, string) {
	runes:= []rune(word)
	letters := []string{}
	for _, r := range runes {
		letters = append(letters, string(r))
	}
	
	rand.Seed(time.Now().UnixNano())
	

	for i := 0; i < int(len(letters)/2); i++ {
		letters[rand.Intn(len(word))] = "_"
	}

	riddle := strings.Join(letters, "")
	
	return riddle, word
}

func playGame(words map[string]string) {
	riddle, original := riddleWord(randWord(words))
	fmt.Println(riddle)
	for i := 1; i < 6; i++ {
		var answer string
		fmt.Print("Enter the answer: ")
		fmt.Scanln(&answer)

		if strings.ToLower(answer) == original {
			fmt.Println("Correct!")
			break
		} else {
			fmt.Println("Wrong!")
		}

		fmt.Println("Chances left:", 5 - i)

		if 5 - i == 0 {
			fmt.Println("The answer was", original)
		}

		if 5 - i <= 3 {
			fmt.Println("Hint:", words[original])
		}
	}
}

func main() {
	fmt.Println("Welcome to the Go Hangman!\n")
	words := map[string]string{"absurd":"something which doesnt makes sense", "abruptly":"suddenly", "abyss":"abyss", "affix":"fix", "avenue":"avenue", "axiom":"rule", "azure":"microsoft", "bandwagon":"drum-wagin", "blizzard":"strom", "bookworm":"keeps stuck to books", "buffet":"self service serving of food"}

	for i := 0; i < 6; i++ {
		playGame(words)
		fmt.Println("\n^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
	}
}