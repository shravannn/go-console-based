package main

import ("fmt"
		"math/rand"
		"time"
		"strings")

func main()  {
	outcomes := [3]string{"rock", "paper", "scissor"}
	randObj := rand.NewSource(time.Now().Unix())
	r := rand.New(randObj)
	
	fmt.Println("WELCOME TO THE GO ROCK PAPER SCISSOR GAME!")
	fmt.Println("Press 'r' for rock, 'p' for paper and 's' for scissor.\n")

	var ties int
	var comp int
	var user int

	for i := 0; i < 10; i++ {
		random := r.Intn(len(outcomes))
		choice := (outcomes[random])

		var userGuess string
		fmt.Print("Enter you choice: ")
		fmt.Scanln(&userGuess)
		
		userGuess = strings.ToLower(userGuess)
		if choice == "rock" && userGuess == "r" {
			fmt.Println("Tie!")
			ties++
		} else if choice == "rock" && userGuess == "p" {
			fmt.Println("You won this time!")
			user++
		} else if choice == "rock" && userGuess == "s" {
			fmt.Println("You lost this time!")
			comp++
		} else if choice == "paper" && userGuess == "r" {
			fmt.Println("You lost this time!")
			comp++
		} else if choice == "paper" && userGuess == "p" {
			fmt.Println("Tie!")
			ties++
		} else if choice == "paper" && userGuess == "s" {
			fmt.Println("You won this time!")
			user++
		} else if choice == "scissor" && userGuess == "r" {
			fmt.Println("You won this time!")
			user++
		} else if choice == "scissor" && userGuess == "p" {
			fmt.Println("Tie!")
			comp++
		} else if choice == "scissor" && userGuess == "s" {
			fmt.Println("Tie!")
			ties++
		} else {
			fmt.Println("Invalid input!")
		}
		fmt.Println("\n-------------------------------------------------\n")
	}

	fmt.Println("Your score:", user)
	fmt.Println("Computer's score:", comp)
	fmt.Println("Ties:", ties)

	if user == comp{
		fmt.Println("THE MATCH RESULTED IN TIE!")
	} else if user > comp {
		fmt.Println("THE MATCH RESULTED IN YOUR VICTORY!")
	} else {
		fmt.Println("THE MATCH RESULTED IN YOUR LOSS!")
	}

}