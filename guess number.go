package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main () {

	r := rand.Intn(100)
	fmt.Println("Welcome to the Go guess the number game!")

	rand.Seed(time.Now().UnixNano())
	n := r + rand.Intn(100-r+1)

	i := 1
	for i < 10 {
		var guess int
		fmt.Print("Guess the number: ")
		fmt.Scanln(&guess)

		if guess > n {
			fmt.Println("Decrease the number!")
			fmt.Println("Chances left:", 10-i)
		} else if guess < n {
			fmt.Println("Increase the number!")
			fmt.Println("Chances left:", 10-i)
		} else {
			fmt.Println("You won the game in", i, "chances!")
			break
		}

		fmt.Println("\n---------------------------------------\n")
		i++
	}
	

}